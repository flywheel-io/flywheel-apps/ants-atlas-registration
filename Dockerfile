FROM flywheel/fw-gear-ants-base:2.3.5

ENV FLYWHEEL="/flywheel/v0"
WORKDIR ${FLYWHEEL}

RUN pip install --no-cache-dir "poetry==1.1.2"

COPY pyproject.toml poetry.lock $FLYWHEEL/
RUN poetry install --no-dev

COPY run.py manifest.json README.md $FLYWHEEL/
COPY fw_gear_ants_atlas_registration $FLYWHEEL/fw_gear_ants_atlas_registration
RUN poetry install --no-dev

RUN chmod a+x $FLYWHEEL/run.py
ENTRYPOINT ["poetry","run","python","/flywheel/v0/run.py"]
