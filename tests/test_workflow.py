import pytest
from nipype.pipeline.engine import Workflow

from fw_gear_ants_atlas_registration.atlas import get_atlas
from fw_gear_ants_atlas_registration.workflow import (
    registration_workflow,
    setup_registration,
)


def test_setup_registration(example_reg_node_config, tmp_path):
    fixed = tmp_path / "fixed"
    moving = tmp_path / "moving"
    moving_m = tmp_path / "moving_m"
    fixed.touch()
    moving.touch()
    moving_m.touch()
    out = setup_registration(fixed, None, moving, moving_m, example_reg_node_config)
    assert out
    assert out.interface.inputs.fixed_image
    assert out.interface.inputs.moving_image
    # Only set if both masks are provided
    assert not out.interface.inputs.fixed_image_mask
    assert not out.interface.inputs.moving_image_mask


def test_setup_registration_with_masks(example_reg_node_config, tmp_path):
    fixed = tmp_path / "fixed"
    moving = tmp_path / "moving"
    fixed_m = tmp_path / "fixed_m"
    moving_m = tmp_path / "moving_m"
    fixed.touch()
    moving.touch()
    fixed_m.touch()
    moving_m.touch()
    out = setup_registration(fixed, fixed_m, moving, moving_m, example_reg_node_config)
    assert out
    assert out.interface.inputs.fixed_image_mask
    assert out.interface.inputs.moving_image_mask
    assert out.interface.inputs.fixed_image
    assert out.interface.inputs.moving_image


def test_setup_workflow(example_reg_node_config, tmp_path, caplog):
    caplog.set_level(0)
    fixed = tmp_path / "fixed"
    moving = tmp_path / "moving"
    fixed.touch()
    moving.touch()
    (tmp_path / "out").mkdir()
    out = setup_registration(fixed, None, moving, None, example_reg_node_config)
    _, _, files, interp = get_atlas("mindboggle")
    wf = registration_workflow((tmp_path / "out"), out, files, interp)
    assert wf
    recs = [
        rec
        for rec in caplog.record_tuples
        if rec[0] == "fw_gear_ants_atlas_registration.workflow"
    ]

    assert recs[0][2] == "Setting up registration workflow"
    assert recs[1][2] == (f"Setting workflow base_dir to {(tmp_path / 'out') / 'work'}")
    assert recs[2][2] == ("Creating MapNode over atlas files and interpolations")
    assert recs[3][2] == "Creating DataSink node"
    assert recs[4][2] == "Connecting nodes"


def test_setup_workflow(example_reg_node_config, tmp_path, caplog, mocker):
    workf = mocker.patch.object(Workflow, "__new__")
    workf.return_value.connect.side_effect = ValueError("error")
    caplog.set_level(40)  # ERROR
    fixed = tmp_path / "fixed"
    moving = tmp_path / "moving"
    fixed.touch()
    moving.touch()
    (tmp_path / "out").mkdir()
    out = setup_registration(fixed, None, moving, None, example_reg_node_config)
    _, _, files, interp = get_atlas("mindboggle")
    with pytest.raises(RuntimeError):
        registration_workflow((tmp_path / "out"), out, files, interp)
    assert caplog.record_tuples[-1][2].startswith("Unhandled exception")
