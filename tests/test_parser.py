"""Module to test parser.py"""
from pathlib import Path
from unittest.mock import MagicMock

import pytest
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_ants_atlas_registration.parser import parse_config


def test_parse_config_simple(caplog):
    caplog.set_level(0)
    gear_context = MagicMock()
    gear_context.config_json = {
        "config": {"debug": True, "atlas": "mindboggle", "transforms": '["test"]'}
    }
    out = parse_config(gear_context)
    assert gear_context.get_input_path.call_count == 2
    assert caplog.record_tuples[0][2].startswith("Using 1 stages:")
    assert caplog.record_tuples[1][2].startswith("Loaded atlas mindboggle")
    assert isinstance(out[0], dict)
    assert isinstance(out[1], Path)
    assert isinstance(out[2], Path)
    assert isinstance(out[3], Path)
    assert isinstance(out[4], Path)
    assert out[5] == "mindboggle"


def test_parse_config_full_ants_options(example_reg_node_config):
    gear_context = MagicMock()
    gear_context.config_json = {
        "config": {
            "debug": True,
            "atlas": "mindboggle",
            "dimension": 3,
            "metric": '["MI","MI"]',
            "metric_weight": "[1,1]",
            "radius_or_number_of_bins": "[32,32]",
            "sampling_strategy": '["Regular","Regular"]',
            "sampling_percentage": "[0.25,0.25]",
            "use_estimate_learning_rate_once": "[false,false]",
            "use_histogram_matching": "[false,false]",
            "interpolation": "Linear",
            "transforms": '["Rigid","Affine"]',
            "transform_parameters": "[0.1,0.1]",
            "number_of_iterations": "[[100,50,25,10],[100,50,25,10]]",
            "smoothing_sigmas": "[[4,3,2,1],[4,3,2,1]]",
            "sigma_units": '["vox","vox"]',
            "shrink_factors": "[[12,8,4,2],[12,8,4,2]]",
            "convergence_threshold": '["1e-6","1e-6"]',
            "convergence_window_size": "[10,10]",
            "collapse_output_transforms": True,
            "initialize_transforms_per_stage": False,
            "float": None,
            "output_transform_prefix": "transform",
            "output_warped_image": True,
            "output_inverse_warped_image": True,
            "winsorize_upper_quantile": 0.995,
            "winsorize_lower_quantile": 0.005,
            "num_threads": 1,
        }
    }
    out = parse_config(gear_context)
    assert out[0] == example_reg_node_config


@pytest.mark.parametrize(
    "in_dict",
    [
        # Transform invalid json
        {
            "debug": True,
            "atlas": "mindboggle",
            "transforms": '[\'Rigid","Affine"]',
        },
        # Other field invalid json
        {
            "debug": True,
            "atlas": "mindboggle",
            "transforms": '["Rigid","Affine"]',
            "metric": '[MI","MI"]',
        },
        # Other field length doesn't match length of transforms
        {
            "debug": True,
            "atlas": "mindboggle",
            "transforms": '["Rigid","Affine"]',
            "metric": '["MI"]',
        },
        # Unknown option raises
        {
            "debug": True,
            "atlas": "mindboggle",
            "transforms": '["Rigid","Affine"]',
            "unknown_option": True,
        },
    ],
)
def test_parse_config_invalid(in_dict):
    gear_context = MagicMock()
    gear_context.config_json = {"config": in_dict}
    with pytest.raises(ValueError):
        parse_config(gear_context)
