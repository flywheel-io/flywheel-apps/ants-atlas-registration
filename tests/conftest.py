import pytest


@pytest.fixture
def example_reg_node_config():
    return {
        "write_composite_transform": True,
        "dimension": 3,
        "metric": ["MI", "MI"],
        "metric_weight": [1, 1],
        "radius_or_number_of_bins": [32, 32],
        "sampling_strategy": ["Regular", "Regular"],
        "sampling_percentage": [0.25, 0.25],
        "use_estimate_learning_rate_once": [False, False],
        "use_histogram_matching": [False, False],
        "interpolation": "Linear",
        "transforms": ["Rigid", "Affine"],
        "transform_parameters": [(0.1,), (0.1,)],
        "number_of_iterations": [[100, 50, 25, 10], [100, 50, 25, 10]],
        "smoothing_sigmas": [[4, 3, 2, 1], [4, 3, 2, 1]],
        "sigma_units": ["vox", "vox"],
        "shrink_factors": [[12, 8, 4, 2], [12, 8, 4, 2]],
        "convergence_threshold": [1e-6, 1e-6],
        "convergence_window_size": [10, 10],
        "collapse_output_transforms": True,
        "initialize_transforms_per_stage": False,
        "output_transform_prefix": "transform",
        "output_warped_image": True,
        "output_inverse_warped_image": True,
        "winsorize_upper_quantile": 0.995,
        "winsorize_lower_quantile": 0.005,
        "num_threads": 1,
        "verbose": True,
    }
