from pathlib import Path

import pytest

from fw_gear_ants_atlas_registration.atlas import get_atlas


@pytest.mark.parametrize(
    "name,files",
    [
        # Mindboggle
        (
            "mindboggle",
            [
                ("priors1.nii.gz", "Linear"),
                ("priors2.nii.gz", "Linear"),
                ("priors3.nii.gz", "Linear"),
                ("priors4.nii.gz", "Linear"),
                ("priors5.nii.gz", "Linear"),
                ("priors6.nii.gz", "Linear"),
                ("T_template0.nii.gz", "Linear"),
                ("T_template0_BrainCerebellum.nii.gz", "Linear"),
                ("T_template0_BrainCerebellumExtractionMask.nii.gz", "NearestNeighbor"),
                ("T_template0_BrainCerebellumMask.nii.gz", "NearestNeighbor"),
                ("T_template0_BrainCerebellumProbabilityMask.nii.gz", "Linear"),
                (
                    "T_template0_BrainCerebellumRegistrationMask.nii.gz",
                    "NearestNeighbor",
                ),
                ("T_template0_glm_4labelsJointFusion.nii.gz", "NearestNeighbor"),
                ("T_template0_glm_6labelsJointFusion.nii.gz", "NearestNeighbor"),
            ],
        )
    ],
)
def test_atlas(name, files):
    _, _, a_files, a_interps = get_atlas(name)
    for i, (file_, interpolation) in enumerate(files):
        assert Path(a_files[i]).name == file_
        assert a_interps[i] == interpolation


def test_atlas_invalid():
    with pytest.raises(ValueError):
        get_atlas("not_an_atlas")
