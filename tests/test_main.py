"""Module to test main.py"""
from pathlib import Path
from unittest.mock import MagicMock

from fw_gear_ants_atlas_registration.main import run, zip_outputs


def test_run(mocker, tmp_path, example_reg_node_config):
    wf = mocker.patch("fw_gear_ants_atlas_registration.main.registration_workflow")
    zipf = mocker.patch("fw_gear_ants_atlas_registration.main.zip_outputs")
    setup = mocker.patch("fw_gear_ants_atlas_registration.main.setup_registration")
    fixed = tmp_path / "fixed"
    moving = tmp_path / "moving"
    moving_m = tmp_path / "moving_m"
    fixed.touch()
    moving.touch()
    moving_m.touch()
    a_files, a_int = MagicMock(), MagicMock()
    run(
        fixed,
        None,
        moving,
        None,
        example_reg_node_config,
        "mindboggle",
        a_files,
        a_int,
        tmp_path,
    )
    setup.assert_called_once_with(fixed, None, moving, None, example_reg_node_config)
    wf.assert_called_once_with(Path("/flywheel/v0"), setup.return_value, a_files, a_int)
    wf.return_value.run.assert_called_once()
    zipf.assert_called_once_with("mindboggle", tmp_path)


def test_zip_outputs(mocker, tmp_path):
    shutil_m = mocker.patch("fw_gear_ants_atlas_registration.main.shutil")
    zipf = mocker.patch("fw_gear_ants_atlas_registration.main.zipfile")
    fileglob = mocker.patch("fw_gear_ants_atlas_registration.main.fileglob")
    fileglob.return_value = [Path("test/priors1.nii.gz")]
    zip_outputs("mindboggle", tmp_path)
    zipf.ZipFile.assert_called_once_with(tmp_path / "mindboggle.zip", "w")
    fileglob.assert_called_once_with(
        tmp_path / "transformed", pattern="*.nii.gz", recurse=True
    )
    zipcm = zipf.ZipFile.return_value.__enter__.return_value
    zipcm.write.assert_called_once_with(
        Path("test/priors1.nii.gz"), arcname="mindboggle/Priors2/priors1.nii.gz"
    )
    shutil_m.rmtree.assert_called_once_with(tmp_path / "transformed")
