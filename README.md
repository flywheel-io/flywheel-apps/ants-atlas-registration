# ANTs Atlas Registration

This performs the registration of predefined atlases to a target
image (fixed image) using the antsRegistration algorithm from the ANTs
package. The base template from the atlas dataset will be registered
to the fixed image provided by the user and the computed warping will
be applied to transform all atlas derivative images (e.g. masks, priors)
into the fixed image space.

![Workflow](./docs/images/ants-atlas-registration.png)

## Usage

### Inputs

* __fixed_image__: NIfTI file to be used as the fixed image in `antsRegistration`.
* __fixed_image_mask__: Optional NIfTI mask for the `fixed_image`

### Configuration

#### Gear config

* __debug__ (boolean, default False): Include debug statements in output.
* __atlas__: (string, default mindboggle): Atlas to register to `fixed_image`.
  Currently only supporting `mindboggle`

#### ANTs config

##### Single valued options

* __dimension__: (int, default 3): Image dimension, 2 or 3.
* __interpolation__: (string, default Linear) One of Linear, NearestNeighbor,
  CosineWindowedSinc, WelchWindowedSinc. HammingWindowedSinc, LanczosWindowedSinc,
  BSpline, MultiLabel, Gaussian
* __interpolation_parameters__: (JSON string) Optional parameters for interpolation
  method. Ex. if Interpolation is Gaussian, you may set sigma and alpha with `"[.1,1]"`
  which can be parsed by a JSON parser.
* __collapse_output_transforms__: (boolean, default True) Collapse output transforms.
  Specifically, enabling this option combines all adjacent linear transforms and
  composes all adjacent displacement field transforms before writing the results to
  disk.
* __initialize_transforms_per_stage__: (boolean, default False) Initialize linear
  transforms from the previous stage. By enabling this option, the current linear stage
  transform is directly intialized from the previous stages linear transform; this
  allows multiple linear stages to be run where each stage directly updates the
  estimated linear transform from the previous stage. (e.g. Translation -> Rigid ->
  Affine)
* __float__: (boolean, default False) Use float instead of double for computations.
* __output_transform_prefix__: (string, default `"transform"`) Output transform prefix, only
  if output_warped_image is True
* __output_warped_image__: (boolean, default True) Output warped moving image to the
  fixed space and
* __output_inverse_warped_image__: (boolean, default True) Output warped fixed image to
  the fixed space and
* __winsorize_upper_quantile__: (number, default 0.995) Winsorize data based on
  specified upper quantile
* __winsorize_lower_quantile__: (number, default 0.005) Winsorize data based on
  specified lower quantile
* __num_threads__: (integer, default 1) Number of ITK threads to use.
* __args__: (string, default None) Additional arguments

##### Stage specific options

Each of the stage specific options must be a JSON string that parses into a list of the
same length as the number of `transforms`.  Each entry in this list is the value for a
single stage.

* __metric__: (string, default ["MI","MI","CC"]) List of items which are
  CC or MeanSquares or Demons or GC or MI or Mattes.  The metric(s) to use for each
  stage. Enclose a list of the previous values in square brackets, to use multiple
  metrics for a single stage i.e. `["MI","CC",["MI,"CC"]]`
* __metric_weight__: (string, default [1,1,1])  The metric weight(s) for
  each stage (float, weights must sum to 1 per stage).  Shape must match the config
  value `metric`
* __radius_or_number_of_bins__: (string, default [32,32,32])  The number of
  bins in each stage for the MI and Mattes metric, the radius for other metrics
  (integer). Shape must match the config value `metric`
* __sampling_strategy__: (string, default ["Regular","Regular","Regular"])
  The metric sample strategy for each stage (one of '', 'Regular', or 'Random').  Shape
  must match the config value `metric`
* __sampling_percentage__: (string, default [0.25,0.25,0.25])  The metric
  sampling percentage(s) to use for each stage (float, 0 <= val <= 1). Shape must match
  the config value `metric`
* __use_estimate_learning_rate_once__: (string, default [false,false,false]) (JSON
  string) Estimate the learning rate step size only at the beginning of each level
  (boolean).  Must match number of stages.
* __use_histogram_matching__: (string, default [false,false,false])
  Histogram match the images before registration (boolean).  Must match number of
  stages.
* __interpolation__: (string, default Linear)  One of Linear,
  NearestNeighbor, CosineWindowedSinc, WelchWindowedSinc. HammingWindowedSinc,
  LanczosWindowedSinc, BSpline, MultiLabel, Gaussian
* __interpolation_parameters__: (string, default None)  Parameters for
  interpolation method. (json string)
* __transforms__: (string, default ["Rigid","Affine","SyN"])  A list of
  items which are one of Rigid, Affine, CompositeAffine, Similarity, Translation,
  BSpline, GaussianDisplacementField, TimeVaryingVelocityField,
  TimeVaryingBSplineVelocityField, SyN, BSplineSyN, Exponential, or BSplineExponential.
  Must be the same size as number of Stages.
* __transform_parameters__: (string, default [0.1,0.1,[0.1,3.0,0.0]])
  Parameters for transform at each stage.  Must be same size as number of stages.
* __restrict_deformation__: (string, default None) A list of lists, one for each stage.
  At each stage items are 0.0 <= a floating point number <= 1.0. This option allows the
  user to restrict the optimization of the displacement field, translation, rigid or
  affine transform on a per-component basis. For example, if one wants to limit the
  deformation or rotation of 3-D volume to the first two dimensions, this is possible by
  specifying a weight vector of 1,1,0 for a deformation field or 1,1,0,1,1,0 for a rigid
  transformation. Low-dimensional restriction only works if there are no preceding
  transformations.
* __number_of_iterations__: (string, default
  [[1000,500,250,100],[1000,500,250,100],[100,100,70,50,20]])  List of
  lists of integers for each stage, corresponds to MxNxO in the antsRegistration
  documentation: Convergence is determined from the number of iterations per level and
  is determined by  fitting a line to the normalized energy profile of the last N
  iterations (where N is specified by the window size) and determining the slope which
  is then compared with the convergence threshold.
* __smoothing_sigmas__: (string, default [[4,3,2,1],[4,3,2,1],[5,3,2,1,0]]) (JSON
  string) List of lists of floats for each stage.  Specify the sigma of gaussian
  smoothing at each level.
* __sigma_units__: (string, default ["vox","vox","vox"]) Corresponding units of the
  smoothing sigmas, either mm or vox
* __shrink_factors__: (string, default [[12,8,4,2],[12,8,4,2],[10,6,4,2,1]]) (JSON
  string) List of list of integers for each stage. Specify  the  shrink  factor  for the
  virtual domain (typically the fixed image) at each level.
* __convergence_threshold__: (string, default ["1e-6","1e-6","1e-6"])  List
  of string valued convergence thresholds, one for each stage.
* __convergence_window_size__: (string, default [10,10,10])  List of
  integer window sizes, one for each stage.

## Contributing

For more information about how to get started contributing to this gear,
checkout [CONTRIBUTING.md](CONTRIBUTING.md).
