#!/usr/bin/env python
"""The run script"""
import logging
import sys

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_ants_atlas_registration.main import run
from fw_gear_ants_atlas_registration.parser import parse_config

log = logging.getLogger(__name__)


def main(context: GearToolkitContext) -> None:  # pragma: no cover
    """Parses config and run"""

    (
        conf,
        fixed_image,
        fixed_mask,
        moving_image,
        moving_mask,
        atlas_name,
        atlas_paths,
        atlas_interpolations,
    ) = parse_config(context)

    e_code = run(
        fixed_image,
        fixed_mask,
        moving_image,
        moving_mask,
        conf,
        atlas_name,
        atlas_paths,
        atlas_interpolations,
        gear_context.output_dir,
    )

    # Exit the python script (and thus the container) with the exit
    # code returned by example_gear.main.run function.
    sys.exit(e_code)


# Only execute if file is run as main, not when imported by another module
if __name__ == "__main__":  # pragma: no cover
    # Get access to gear config, inputs, and sdk client if enabled.
    with GearToolkitContext() as gear_context:

        # Initialize logging, set logging level based on `debug` configuration
        # key in gear config.
        gear_context.init_logging()

        # Pass the gear context into main function defined above.
        main(gear_context)
