# Release notes

## 0.1.1

Maintenance:

* Change structure of output folder to be consistent with what ants-longitudinal-dbm
  expects.

## 0.1.0

Initial Release
